Title: My First Review
Date: 2010-12-03 10:20
Category: Review

$$\left| \nabla \phi \right| = 1$$
Doloremque vero modi sed adipisci non. Soluta qui ipsam voluptas molestias eos. Asperiores laboriosam id amet neque. Voluptatibus exercitationem quo harum veritatis corporis ducimus sapiente nostrum.

    :::python
    print("The triple-colon syntax will *not* show line numbers.")

Voluptatem ea id eligendi dolorem explicabo rerum delectus enim. Expedita ut vero magni. Dignissimos sequi soluta voluptatem.

$$
    W'_{bal}(t) = W' - \sum_{u=0}^t W'_{exp}(u) \cdot e^{-\frac{t-u}{\tau_{W'}}}$$
$$
    tau_{W'} = 546 \cdot e^{-0.01 \cdot D_{CP}}+316
$$
$$
    W'_{exp}(t) = 
        \begin{cases}
            0 & \text{where } P(t) < CP,
            \newline P(t) - CP & \text{else}
    \end{cases},
$$

Animi fugit et voluptas in explicabo quae. Ut natus ut expedita et recusandae et et labore. Excepturi tenetur ea aut ullam alias voluptate. Voluptates iste suscipit delectus. Iusto dolores sit esse quae voluptatum quia.
